var main = (function(canvasOptions) {

	/* Inner Variables */
	var socket = io();
	var canvas = canvasOptions.canvas;
	canvas.width = canvasOptions.width;
	canvas.height = canvasOptions.height; 
	var context = canvas.getContext('2d');
	var before = new Date().getTime() - 10;
	var onUpdateList = [];
	var objectList = [];
	var onUpdateContext = [];
	var clearColor = "rgb(0,2,6)";

	/* Function Definitions */
	function Update() {
		ClearScreen();
		var now = new Date().getTime();
		var deltaTime = now - before;
		before = now;

		var updateOptions = {
			deltaTime: deltaTime,
			socket: socket,
			context: context
		};

		onUpdateList.forEach(
			function(anOnUpdateFunction) { 
				try {
					anOnUpdateFunction.call(onUpdateContext[anOnUpdateFunction], updateOptions); 
				}
				catch(error) {
					console.error("Error OnUpdate: " + error);
					exposedFunctions.RemoveOnUpdate(anOnUpdateFunction);
				}
			} 
		);
		
		objectList.forEach(
			function(anObject) { 
				try {
					anObject.Update(updateOptions); 
				}
				catch(error) {
					console.error("Error OnUpdate Object: " + error);
					exposedFunctions.RemoveObject(anObject);
				}
			} 
		);

		requestAnimationFrame(Update);
	}

	function ClearScreen() {
		context.save();
			context.fillStyle = clearColor;
			context.fillRect(0, 0, canvasOptions.width, canvasOptions.height);
		context.restore();
	}

	/* Input Event Handling */
	window.onkeyup = function(e) {};
	window.onkeydown = function(e) {};

	socket.on("message", function(message) {
		eval(message)();
	});

	// Start of Updat Loop
	requestAnimationFrame(Update);

	/* To Return Object */
	var exposedFunctions = {
		AddOnUpdate: function(aFunction, aContext) {
			onUpdateList.push(aFunction);
			if(aContext === undefined || aContext === null) {
				aContext = {};
			}
			onUpdateContext[aFunction] = aContext;
			return this;
		},
		ClearOnUpdate: function() {
			onUpdateList = [];
			onUpdateContext = [];
			return this;
		},
		RemoveOnUpdate: function(aFunction) {
			if(onUpdateList.indexOf(aFunction) != -1 ? onUpdateList.splice(onUpdateList.indexOf(aFunction), 1) : false) {
				delete onUpdateContext[aFunction];
			}
			return this;
		},
		AddObject: function(anObject) {
			objectList.push(anObject);
			return this;
		},
		RemoveObject: function(anObject) {
			var index = objectList.indexOf(anObject)
			if(index != -1) 
				objectList.splice(index, 1);
			return this;
		},
		ClearObjects: function() {
			objectList = [];
			return this;
		},
		SetClearColor: function(newColor) {
			clearColor = newColor;
			return this;
		}
	};

	return exposedFunctions;
})({
	width: 800, 
	height: 600,
	canvas: document.getElementById('main_canvas'),
});