var app = require('express')();
var fs = require('fs');
var static = require('node-static');
var file = new static.Server("./client");

var io = require('socket.io')(
  require('http').createServer(function(request, response){
    request.addListener('end', function(){
      file.serve(request, response, function (error, result) {
          if (error) {
              console.log("Error serving " + request.url + " - " + error.message);
              response.writeHead(error.status, error.headers);
              response.end();
          }
      });
    }).resume();
  }).listen(1337))

io.on('connection', function(socket){
    console.log('a user connected: ' + socket.id);
    socket.on('disconnect', function(){
      console.log('user disconnected: ' + socket.id);
    });

    socket.emit("message", "(function(){ console.log(this); })");
});

var i = 0;